<%@page contentType="text/html" pageEncoding="windows-1250"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1250">
        <link href="<c:url value='/static/style.css' />" rel="stylesheet">
        <title>Serverov� ��st</title>
    </head>
    <body>
        <h1>Server</h1>
        <form:form method="POST">
            <input type="submit" value="Test vlo�en�">
        </form:form>
        <div>
            <h4>Obsah DB:</h4>
            <p>Po�et z�znam�: ${msgCount}</p>
            <table>
                <tr>
                    <th>ID</th>
                    <th>Odesl�no</th>
                    <th>Text</th>
                </tr>
                <c:forEach items="${messages}" var="msg">
                    <tr>
                        <td><c:out value="${msg.id}"/></td>
                        <td><c:out value="${msg.getTimestampFormatted()}"/></td>
                        <td><c:out value="${msg.text}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>
