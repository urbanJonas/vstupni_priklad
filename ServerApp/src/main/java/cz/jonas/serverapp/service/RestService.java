/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jonas.serverapp.service;

import cz.jonas.shared.model.Message;
import cz.jonas.shared.service.ResponseService;
import java.util.Random;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jonas
 */
@Service("restService")
@Path("/response")                                                              //sluzby zde navrzene budou na URI [host]/ServerApp/rest/response/[sluzba]
public class RestService implements ResponseService {

    @Autowired
    private MessageService dbService;

    private static final Logger LOG = Logger.getLogger(RestService.class);

    @Override
    @POST                                                                       //sluzba bude volana POST pozadavkem...
    @Path("/get")                                                               //...na URI localhost:8080/ServerApp/rest/response/get
    @Consumes(MediaType.APPLICATION_XML)                                        //sluzba prijima objekt serializovany do XML (viz anotace u tridy Message)
    @Produces(MediaType.APPLICATION_XML)                                        //sluzba vraci objekt serializovany do XML
    public Message getResponse(Message msg) {
        LOG.debug("sluzba zavolana");
        if (msg == null) {
            LOG.error("prijata zprava je null. Neco bude spatne");
            return null;
        }

        //todo tady to hapruje:
        //naautowirovana dbService v tehle tride je null (pri "pohledu" od klienta).
        //Kdyz jsem ale zkusil tuto tridu naautowirovat do serveroveho controlleru
        //(serverapp.controller.homepageController) a z nej otestovat 'dbService'
        //na null, vse bylo OK. Tusim ze za to muze vytvareni instance tehle sluzby
        //v RestCofig pres new, coz se Springu nelibi, nemuzu ale vymyslet funkcni workaround :(
        if (dbService != null) {
            LOG.info("ukladani zpravy do DB");
            dbService.saveMessage(msg);
        } else {
            LOG.warn("dbService je null, zprava se neulozi.");
        }

        LOG.debug("generovani odpovedi");
        Message rsp = new Message();
        //todo nejake generovani odpovedi
        rsp.setText("Odpověď na: \"" + msg.getText() + "\"");
        return rsp;
    }

    private final Random rnd = new Random();

    @Override
    @GET
    @Path("/test")
    @Produces(MediaType.TEXT_PLAIN)
    public String getTest() {
        return "par random cisel: " + rnd.nextInt() + ", " + rnd.nextInt();
    }
}
