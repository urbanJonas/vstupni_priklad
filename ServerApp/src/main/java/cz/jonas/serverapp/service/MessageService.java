package cz.jonas.serverapp.service;

import cz.jonas.shared.model.Message;
import java.util.List;

/**
 * rozhrani sluzby zprostredkovavajici pristup k persistencni vrstve
 * @author Jonas
 */
public interface MessageService {
    
    void saveMessage(Message message);
 
    List<Message> getAllMessages();
    
}
