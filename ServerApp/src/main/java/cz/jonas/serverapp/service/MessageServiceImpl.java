/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jonas.serverapp.service;

import cz.jonas.serverapp.dao.MessageDao;
import cz.jonas.serverapp.mappers.TableExistsMapper;
import cz.jonas.shared.model.Message;
import java.util.List;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jonas
 * @Service: trida je poskytovatelem sluzby, v tomto pripade (asi, pokud to
 * chapu spravne) zprostredkovani pristupu k DB.
 * @Transactional: kazda metoda v teto tride pri svem zavolani zahaji transakci
 * nad pripojenym DAO. Pokud metoda dobehne uspesne, transakce se potvrdi
 * (commit) a ukonci, v pripade selhani se provede rollback
 */
@Service("dbService")
@Transactional
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageDao dao;

    @Autowired
    private TableExistsMapper tableExistsMapper;

    //Logger
    private static final Logger LOG = Logger.getLogger(MessageServiceImpl.class);

    //vsechny metody volaji jiz implementovane metody v MessageDaoImpl (pres interface MessageDao)
    @Override
    public void saveMessage(Message message) {
        //kontrola existence tabulky v DB, prip. vytvoreni
        LOG.debug("kontrola existence tabulky");
        try {
            tableExistsMapper.getRowCount();
            LOG.debug("tabulka je v poradku");
        } catch (PersistenceException ex) {
            LOG.error("Exception - " + ex.getMessage());
            LOG.info("vytvarim tabulku...");
            tableExistsMapper.createTable();
        }
        dao.saveMessage(message);
    }

    @Override
    public List<Message> getAllMessages() {
        LOG.info("ziskavani zprav z DB");
        //kontrola existence tabulky v DB, prip. vytvoreni
        LOG.debug("kontrola existence tabulky");
        try {
            tableExistsMapper.getRowCount();
            LOG.debug("tabulka je v poradku");
        } catch (PersistenceException ex) {
            LOG.error("Exception - " + ex.getMessage());
            LOG.info("vytvarim tabulku...");
            tableExistsMapper.createTable();
        }
        return dao.getAllMessages();
    }

}
