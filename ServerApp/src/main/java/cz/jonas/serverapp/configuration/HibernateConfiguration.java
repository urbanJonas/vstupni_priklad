package cz.jonas.serverapp.configuration;

import java.util.Properties;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Configuration: znaci, ze se jedna o konfiguracni tridu obsahujici jednu ci vice metod s anotaci @Bean
 * @ComponentScan: urcuje balicek, ve kterem se nachazi definice Beanu a trid, se kterymi spring v teto appce bude pracovat
 * @EnableTransactionManagement: Umoznuje springu pouzivat anotace souvisejici s transakcnimi operacemi
 * @PropertySource: viz doc metody sessionFactory()
 */
@Configuration
@EnableTransactionManagement
@ComponentScan({ "cz.jonas.serverapp.configuration" })
@PropertySource(value = { "classpath:application.properties" })
public class HibernateConfiguration {
    
    @Autowired
    private Environment environment;
    
    /**
     * nahrada za XML konfiguracni soubor. Diky @PropertySource anotaci lze samotnou konfiguraci umistit do externiho
     * .properties souboru umisteneho tam, kam anotace odkazuje.
     * Pomoci globalni promenne environment dokaze Spring tento soubor najit a vytahat z nej potrebne info pro:
     *      Zkonstruovani objektu DataSource, ktery vyzaduje obecne pripojovaci udaje (conn string, jmeno a heslo)
     *      Vytvoreni mensi sady Properties, ktere obsahuji pouze hibernate-specificke nastaveni,
     *          a ktera se pak nastavi k SessionFactory
     * @return
     */
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        //nastaveni noveho DataSource vytvoreneho z konfiguracniho souboru
        sessionFactory.setDataSource(dataSource());
        //nastaveni cesty k tridam objektu, ktere se budou do DB ukladat
        sessionFactory.setPackagesToScan(new String[]{"cz.jonas.shared.model"});
        //nastaveni hibernate-specificke konfigurace
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }
    
    /**
     * Metoda pro vytvoreni objektu DataSource
     * @return 
     */
    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
        return dataSource;
    }
    
    /**
     * Metoda pro vytvoreni Properties objektu obsahujiciho konfiguraci hibernate
     * @return 
     */
    @Bean
    public Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
        properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
        properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
        return properties;
    }
    
    /**
     * Do teto metody je eventuelne vlozena sessionFactory z metody sessionFactory(),
     * pomoci niz pak lze provadet trasakcni operace
     * @param s sessionFactory
     * @return 
     */
    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory s) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(s);
        return txManager;
    }
}
