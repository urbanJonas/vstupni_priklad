/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jonas.serverapp.configuration;

import cz.jonas.serverapp.service.RestService;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Jonas
 */
@Configuration
@ApplicationPath("/rest")    //sluzby budou pristupne na URI locahost:8080/ServerApp/rest/[sluzba]
public class RestConfig extends Application {
    
    private Set<Object> singletons = new HashSet<>();
    
    public RestConfig() {
        singletons.add(new RestService());
    }
    
    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
    
}
