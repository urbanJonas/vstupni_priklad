/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jonas.serverapp.configuration;

import cz.jonas.serverapp.mappers.TableExistsMapper;
import javax.sql.DataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Jonas
 */
@Configuration
public class MyBatisConfiguration {
    
    @Autowired
    private DataSource dataSource;
    
    //Logger
    private static final Logger LOG = Logger.getLogger(MyBatisConfiguration.class);
    
    private SqlSessionFactory sqlSessionFactory(){
        LOG.debug("tvorba sqlSessionFactory");
        TransactionFactory tf = new JdbcTransactionFactory();
        org.apache.ibatis.session.Configuration config = 
                new org.apache.ibatis.session.Configuration(new Environment("messagesDB", tf, dataSource));
        config.addMapper(TableExistsMapper.class);
        return new SqlSessionFactoryBuilder().build(config);
    }
    
    private SqlSession session(){
        LOG.debug("otervreni sqlSession");
        return sqlSessionFactory().openSession(true);
    }
    
    @Bean
    public TableExistsMapper tableExistsMapper(){
        LOG.debug("ziskani mapperu pro kontrolu existence tabulky");
        return session().getMapper(TableExistsMapper.class);
    }
    
}
