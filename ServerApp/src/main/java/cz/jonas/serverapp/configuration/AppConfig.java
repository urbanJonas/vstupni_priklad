package cz.jonas.serverapp.configuration;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 *
 * @author Jonas
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"cz.jonas.serverapp", "cz.jonas.shared"})
public class AppConfig extends WebMvcConfigurerAdapter {

    //Logger
    private static final Logger log = Logger.getLogger(AppConfig.class);

    /**
     * Metoda pro nalezeni pohledu (jsp) ve strukture projektu
     *
     * @return
     */
    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver vr = new InternalResourceViewResolver();
        vr.setViewClass(JstlView.class);
        vr.setPrefix("/WEB-INF/views/");
        vr.setSuffix(".jsp");
        return vr;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry reg) {
        //handler - pattern pozadavku na soubor(y)
        //location - umisteni ve strukture webapp
        reg.addResourceHandler("/static/**").addResourceLocations("/static/");
    }

}
