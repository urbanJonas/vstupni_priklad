/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jonas.serverapp.mappers;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.exceptions.PersistenceException;

/**
 *
 * @author Jonas
 */
public interface TableExistsMapper {

    /**
     * DDL prikaz pro tvorbu tabulky v pripade, ze neexistuje
     */
    @Update("CREATE SEQUENCE IF NOT EXISTS msg_ids; "
          + "CREATE TABLE IF NOT EXISTS messages ("
            + "id INTEGER DEFAULT nextval('msg_ids'), sent TIMESTAMP NOT NULL, text VARCHAR(255) NOT NULL, PRIMARY KEY(id))")
    void createTable();

    /**
     * protoze DDL prikazy nic nevraci, neni z metody createTable() jak zjistit,
     * jestli skutecne doslo k vytvoreni tabulky, nebo jiz existovala. Pokud
     * tabulka neexistuje, tato metoda hodi PersistenceException, ktera se pri volani
     * zachyti, podle toho se zaloguje vysledek kontroly a prip. se zavola
     * metoda createTable().
     *
     * @return pocet radku v tabulce
     * @throws PersistenceException v pripade, ze tabulka neexistuje (nebo pri
     * jakekoliv jine chybe, ale kvuli tehle to tu je... :D)
     */
    @Select("SELECT count(*) FROM messages")
    int getRowCount() throws PersistenceException;

}
