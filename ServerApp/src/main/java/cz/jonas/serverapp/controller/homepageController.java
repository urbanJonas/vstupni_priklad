/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jonas.serverapp.controller;

import cz.jonas.serverapp.service.MessageService;
import cz.jonas.shared.model.Message;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Jonas
 */
@Controller
@RequestMapping("/")
public class homepageController {
    
    @Autowired
    private MessageService dbService;
    
    //Logger
    private static final Logger LOG = Logger.getLogger(homepageController.class);
    
    @RequestMapping(method = RequestMethod.GET)
    public String list(ModelMap mm){
        //to, co se opravdu patri...
        List<Message> messages = dbService.getAllMessages();
        mm.addAttribute("msgCount", messages.size());
        mm.addAttribute("messages", messages);
        return "server";
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public String testAdd(ModelMap mm){
        LOG.debug("test vkladani");
        Message msg1 = new Message();
        Message msg2 = new Message();
        Message msg3 = new Message();
        msg1.setText("test. zprava 1");
        msg2.setText("test. zprava 2");
        msg3.setText("test. zprava 3");
        dbService.saveMessage(msg1);
        dbService.saveMessage(msg2);
        dbService.saveMessage(msg3);
        List<Message> messages = dbService.getAllMessages();
        mm.addAttribute("msgCount", messages.size());
        mm.addAttribute("messages", messages);
        return "server";
    }
    
}
