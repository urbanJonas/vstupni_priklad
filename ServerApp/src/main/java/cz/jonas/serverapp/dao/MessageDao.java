/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jonas.serverapp.dao;

import cz.jonas.shared.model.Message;
import java.util.List;

/**
 *
 * @author Jonas
 */
public interface MessageDao {
    
    void saveMessage(Message message);
    
    List<Message> getAllMessages();
    
}
