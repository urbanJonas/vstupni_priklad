package cz.jonas.serverapp.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * DAO - Data Access Object - jakesi rozhrani mezi aplikaci a persistencni
 * vrstvou (DB, File system, ...). implementuje zakladni operace - uloz a smaz,
 * z teto tridy pak mohou dedit vsechny DAO tridy entit
 *
 * @author Jonas
 */
public abstract class AbstractDao {

    // ziskano z HibernateConfiguration tridy
    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void persist(Object entity) {
        getSession().persist(entity);
    }

    public void delete(Object entity) {
        getSession().delete(entity);
    }
}
