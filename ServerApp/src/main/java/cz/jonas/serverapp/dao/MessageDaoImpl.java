package cz.jonas.serverapp.dao;

import cz.jonas.shared.model.Message;
import java.util.List;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

/**
 * Konkretni implementace DAO tridy pro objekt Message
 * Trida dedi z AbstractDao obecne metody 'uloz' a 'vymaz' a implementuje metody
 * specificke pro objekt Message z interface MessageDao
 * @author Jonas
 * @Repository - trida ma funkci zapouzdreni ukladani, vyhledavani a uchovavani objektu - emuluje kolekci
 * @author Jonas
 */
@Repository
public class MessageDaoImpl extends AbstractDao implements MessageDao {

    @Override
    public void saveMessage(Message message) {
        //volani zdedene metody...
        persist(message);
    }

    @Override
    public List<Message> getAllMessages() {
        //Criteria slouzi pro restrikci vysledku. Instance podle tridy omezi vysledky na tyto objekty
        //Na Criteria pak lze metodou .add(Restrictions.*) pridavat omezeni jako napr. ve where cause v DB dotazu
        //vysledek se pak vrati metodou .list()
        Criteria crit = getSession().createCriteria(Message.class);
        return crit.list();
    }
    
}
