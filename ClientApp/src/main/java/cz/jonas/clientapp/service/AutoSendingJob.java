package cz.jonas.clientapp.service;

import cz.jonas.shared.model.Message;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author Jonas
 */
public class AutoSendingJob extends QuartzJobBean {

//    @Autowired
    private MessageSendingService sendingService;

    private static final Logger log = Logger.getLogger(AutoSendingJob.class);

    private static int counter = 0;

//    public void execute(JobExecutionContext jec) throws JobExecutionException {
    @Override
    public void executeInternal(JobExecutionContext jec) throws JobExecutionException {
        log.debug("AUTOMAT");
        Message msg = new Message();
        msg.setText("Aut. zprava c. " + ++counter);
        log.debug("odesilani autozpravy: " + msg.getText());
        //zavolani sluzby pro odeslani zpravy serveru
        Message rsp = sendingService.sendMessage(msg);
        log.debug("odpoved na autozpravu: " + rsp.getText());
        //todo neco s rsp
    }
    
    public void setSendingService(MessageSendingService sendingService) {
        this.sendingService = sendingService;
    }

}
