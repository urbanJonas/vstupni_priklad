/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jonas.clientapp.service;

import cz.jonas.shared.model.Message;
import cz.jonas.shared.service.ResponseService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jonas
 */
@Service("sendingService")
public class MessageSendingService {

    //RESTEasy
    @Autowired
    private ResponseService proxy;

    //historie
    @Autowired
    private History history;

    private static final Logger LOG = Logger.getLogger(MessageSendingService.class);

    public Message sendMessage(Message msg) {
        LOG.debug("vkladani zpravy do historie...");
        history.addMessage(msg);

        LOG.info("odesilani zpravy");
        Message rsp = null;
        if (proxy == null) {
            LOG.error("proxy null - bezi server?");
        } else {
            LOG.debug("proxy test: " + proxy.getTest());
            rsp = proxy.getResponse(msg);
        }
        if (rsp != null) {
            LOG.info("odpoved ziskana");
        } else {
            LOG.warn("zadna odpoved :(");
        }
        return rsp;
    }

}
