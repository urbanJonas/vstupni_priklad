/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jonas.clientapp.service;

import cz.jonas.shared.model.Message;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 *
 * @author Jonas
 */
@Component
public class History {
    
    private List<Message> history = new ArrayList<>();

    private static final Logger log = Logger.getLogger(MessageSendingService.class);
    
    public List<Message> getHistory() {
        return history;
    }
    
    public void addMessage(Message msg) {
        history.add(0, msg);
    }
}
