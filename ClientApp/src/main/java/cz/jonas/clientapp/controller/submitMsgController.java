package cz.jonas.clientapp.controller;

import cz.jonas.clientapp.service.History;
import cz.jonas.clientapp.service.MessageSendingService;
import cz.jonas.shared.model.Message;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller osetrujici vstupy
 *
 * @author Jonas
 * @Controller - ...ze je to controller...
 * @RequestMapping - pattern, podle ktereho se urcuje, jestli ma tato trida
 * zpracovat v nektere z metod pozadavky - "/" vyhovuje vsem, coz je logicke,
 * vzhledem k tomu ze je tu jen jediny controller
 */
@Controller
@RequestMapping("/")
public class submitMsgController {

    //Historie zprav
    @Autowired
    private History history;
    
    //Sluzba pro odesilani zprav
    @Autowired
    private MessageSendingService sendingService;
    
    //Logger
    private static final Logger log = Logger.getLogger(submitMsgController.class);

    /**
     * Metoda pro pripraveni stranky (pohledu) pri "beznem nacteni" (get)
     *
     * @param mm pokud chapu spravne - jakysi container pro atributy (datove
     * polozky), ktere se na strance pouzivaji
     * @return nazev pohledu (stranky), ktera se ma zobrazit po vykonani
     * requestu
     */
    @RequestMapping(method = RequestMethod.GET)
    public String prepare(ModelMap mm) {
        log.debug("vytvareni a bindovani prazdne zpravy");
        Message msg = new Message();        //novy prazdny objekt zpravy
        mm.addAttribute("message", msg);    //vlozi se do containeru
        mm.addAttribute("msgHistory", history.getHistory());
        log.info("stranka pripravena");   //logovaci zaznam
        return "msgview";                   //vrati se nazev pohledu, ktery se ma zobrazit
    }

    /**
     * Metoda pro zpracovani vlozenych dat, formular v jsp nema specifikovanou
     * akci, jenom metodu POST, proto vyhovuje requestu "/"
     *
     * @param msg Zprava, ktera byla "do stranky vlozena" metodou prepare().
     * Anotace @Valid znaci, ze data ziskana z formulare a doplnena do objektu
     * musi projit validacni kontrolou definovanou anotacemi v tride zpravy
     * (model.Message)
     * @param result vysledek validacni kontroly
     * @param mm stejne jako u prepare()
     * @return nazev pohledu, ktery se ma zobrazit po vykonani requestu
     */
    @RequestMapping(method = RequestMethod.POST)
    public String sendMsg(@Valid Message msg, BindingResult result, ModelMap mm) {
        log.debug("MANUAL");
        if (!result.hasErrors()) {      //pokud pri validaci nedoslo k chybam...
            //zalogovani uspesne operace
            log.info("kontrola zpravy: OK");
            //ziskani odpovedi pres sluzbu zajistujici dalsi zpracovani a odeslani zpravy
            Message rsp = sendingService.sendMessage(msg);
            log.debug("ziskana odpoved: " + rsp.getText());
            //todo neco s odpovedi
            mm.addAttribute("response", rsp);
        } else {
            //zalogovani chyby
            log.warn("kontrola zpravy: FAIL - " + result.getFieldError().toString());
        }
        //bez ohledu na vysledek tohoto zpracovani vlozim aktualne hisotrii predchozich zprav
        //mm.addAttribute("msgHistory", MessageSendingService.getHistory());
        mm.addAttribute("msgHistory", history.getHistory());
        //vratim nazev pohledu, ve kterem jsou pomoci EL oznacena mista, kde se (a jak) v dokumentu vypise obsah vlozenych atributu
        return "msgview";
    }

}
