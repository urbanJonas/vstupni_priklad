/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jonas.clientapp.configuration;

import cz.jonas.clientapp.service.AutoSendingJob;
import cz.jonas.clientapp.service.MessageSendingService;
import cz.jonas.shared.service.ResponseService;
import java.util.Map;
import javax.ws.rs.core.UriBuilder;
import org.apache.log4j.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.quartz.JobDataMap;
import org.springframework.beans.factory.support.ManagedMap;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * Spring konfigurace
 *
 * @author Jonas
 * @Configuration - konfiguracni trida
 * @ComponentScan - kde hledat definice Beanu
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"cz.jonas.clientapp", "cz.jonas.shared"})
public class AppConfig extends WebMvcConfigurerAdapter {

    private static final Logger log = Logger.getLogger(AppConfig.class);

    /**
     * Metoda pro nalezeni pohledu (jsp) ve strukture projektu
     *
     * @return
     */
    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver vr = new InternalResourceViewResolver();
        vr.setViewClass(JstlView.class);
        vr.setPrefix("/WEB-INF/views/");
        vr.setSuffix(".jsp");
        return vr;
    }

    /**
     * nastavuje zdrojovy .properties soubor pro chybove hlasky ve validaci
     *
     * @return
     */
    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource src = new ResourceBundleMessageSource();
        src.setBasename("messages");    //nazev souboru .properties s hlaskami
        return src;
    }

    /**
     * Metoda pro nalezeni dalsich zdrojovych souboru tykajicich se webove
     * prezentace - css, javascripty, atd
     *
     * @param reg
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry reg) {
        //handler - pattern pozadavku na soubor(y)
        //location - umisteni ve strukture webapp
        reg.addResourceHandler("/static/**").addResourceLocations("/static/");
    }

    private final MessageSendingService sendingService = new MessageSendingService();

    @Bean
    public MessageSendingService sendingService() {
        return sendingService;
    }

    //<editor-fold defaultstate="collapsed" desc="RESTEasy">
    @Bean
    public ResponseService proxy() {
        final String uri = "http://localhost:8080/ServerApp/rest";
        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target(uri);
        return target.proxy(ResponseService.class);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Quartz">
    /**
     * Quartz integrovany do Springu
     *
     * @return
     */
    @Bean
    public JobDetailFactoryBean complexJobDetail() {
        JobDetailFactoryBean jdfb = new JobDetailFactoryBean();
        jdfb.setJobClass(AutoSendingJob.class);
        Map<String, MessageSendingService> map = new ManagedMap<>();

        //klic musi byt stejny jako nazev promenne v tride ukolu
        map.put("sendingService", sendingService());

        //inject sendingService do tridy s ukolem
        jdfb.setJobDataMap(new JobDataMap(map));

        //bez tohoto hazi
        //org.quartz.SchedulerException: Jobs added with no trigger must be durable.
        //proc? netusim, kdyz v dalsich beanach (trigger a scheduler) je, alespon podle navodu, spravne :(
        jdfb.setDurability(true);
        return jdfb;
    }

    @Bean
    public SimpleTriggerFactoryBean trigger() {
        SimpleTriggerFactoryBean stfb = new SimpleTriggerFactoryBean();
        stfb.setJobDetail(complexJobDetail().getObject());
        stfb.setRepeatInterval(20000);
        return stfb;
    }

    @Bean
    public SchedulerFactoryBean scheduler() {
        SchedulerFactoryBean sfb = new SchedulerFactoryBean();
        sfb.setJobDetails(complexJobDetail().getObject());
        sfb.setTriggers(trigger().getObject());
        return sfb;
    }
//      </editor-fold>
}
