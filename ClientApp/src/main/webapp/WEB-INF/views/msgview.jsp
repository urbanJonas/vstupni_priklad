<%@page contentType="text/html" pageEncoding="windows-1250"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1250">
        <link href="<c:url value='/static/style.css' />" rel="stylesheet">
        <title>Klientsk� ��st</title>
    </head>
    <body>
        <h1>Klient</h1>
        <div class="formClass">
            <form:form method="POST" modelAttribute="message">
                <div>
                    <label for="textInput">Zpr�va: </label>
                    <form:input type="text" path="text" id="textInput"/>
                    <input type="submit" value="Odeslat">
                    <form:errors path="text" class="errorMsg"/>
                    <p>Odpov��: ${response}</p>
                </div>
            </form:form>
        </div>
        <div>
            <h4>Historie odeslan�ch zpr�v:</h4>
            <table>
                <tr>
                    <th>Odesl�no</th>
                    <th>Text</th>
                </tr>
                <c:forEach items="${msgHistory}" var="msg">
                    <tr>
                        <td><c:out value="${msg.getTimestampFormatted()}"/></td>
                        <td><c:out value="${msg.text}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>
