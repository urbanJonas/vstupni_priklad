/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.jonas.shared.service;

import cz.jonas.shared.model.Message;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Jonas
 */
@Path("/response")                          //sluzby zde navrzene budou dostupne pres URI localhost:8080/[servlet]/rest/response/[sluzba]
public interface ResponseService {

    @POST                                    //sluzba bude volana POST pozadavkem...
    @Path("/get")                           //...na URI localhost:8080/[servlet]/rest/response/get
    @Consumes(MediaType.APPLICATION_XML)    //sluzba prijima objekt serializovany do XML (viz anotace u tridy Message)
    @Produces(MediaType.APPLICATION_XML)    //sluzba vraci objekt serializovany do XML
    Message getResponse(Message msg);
    
    
    @GET
    @Path("/test")
    @Produces(MediaType.TEXT_PLAIN)
    String getTest();
}
