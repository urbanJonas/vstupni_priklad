package cz.jonas.shared.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Jonas
 */
@Entity                                                         //Persistencni vrstva o objektu vi
@Table(name = "MESSAGES")                                       //nazev tabulky, do ktere se tyto objekty ukladaji
@XmlAccessorType(XmlAccessType.FIELD)                           //Objekt je serializovatelny do XML, uchovaji se pole, ostatni prvky pouze pokud budou explicitne anotovany
@XmlType(name = "msg", propOrder = {"text", "timestamp"})       //korenovy prvek XML ponese nazev "msg" a elementy v nem budou ve specifikovanem poradi
@XmlRootElement(name = "msg")
public class Message implements Serializable {

    @Id                                                         //atribut predstavuje primarni identifikator
    @GeneratedValue(strategy = GenerationType.IDENTITY)         //strategie generovani dalsich hodnot
    @XmlTransient                                               //toto pole nebude soucasti serializovaneho XML souboru. ID ma smysl pouze v DB, pro prenos zpravy/odpovedi je nepotrebne
    private int id;

    @Column(name = "text", nullable = false)                    //nazev sloupce pro tento atribut a jeho specifikace (null hodoty, unikatnost, atd)
    @NotEmpty                                                   //validacni anotace
    private String text;

    
    
    @Column(name = "sent", nullable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class) //pouziva se pro typy ktere JPA neumi defaultne zpracovat. Specifikuje se trida, ktera implementuje konverzi mezi typem pouzivanym v aplikaci a SQL typem
    @XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)     //vicemene to same, jen pro serializaci do XML
    private final LocalDateTime timestamp = LocalDateTime.now();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public String getTimestampFormatted() {
        return timestamp.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id;
        hash = 97 * hash + Objects.hashCode(this.text);
        hash = 97 * hash + Objects.hashCode(this.timestamp);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Message other = (Message) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        if (!Objects.equals(this.timestamp, other.timestamp)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[" + timestamp.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")) + "] " + text;
    }
}
