package cz.jonas.shared.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * * Trida pro konverzi mezi aplikacnim typem a DB typem.<br>
 * JPA 2.1 (Java Persistence API) nepodporuje automaticky typy z java.time, proto se vytvorila tato trida, pomoci ktere dokaze
 * JPA provest konverzi.
 * 
 * @Converter - znaci, ze se jedna o konverzni tridu JPA. 
 *      autoApply atribut znaci, jestli se ma konverze pouzit na vsechny vyskyty tohoto typu (LocalDateTime) v aplikaci.
 *      Pokud by byl false, u vybranych atributu je treba pridat anotaci @Convert(converter = 'tridaKonvertoru'.class)
 *      //z nejakeho duvodu mi nefungovalo, tak je ve tride objektu stejne pouzita anotace @Convert...
 * Typove parametry: prvni je dotceny typ v aplikaci (zde LocalDateTime), druhy je jeho ekvivalent v DB (zde java.sql.Timestamp)
 * @author Jonas
 */
@Converter(autoApply = true)
public class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDateTime, Timestamp> {

    /**
     * Metoda pro prevod aplikace -> DB
     * @param attribute
     * @return 
     */
    @Override
    public Timestamp convertToDatabaseColumn(LocalDateTime attribute) {
        return Timestamp.valueOf(attribute);
    }

    /**
     * Metoda pro prevod DB -> aplikace
     * @param dbData
     * @return 
     */
    @Override
    public LocalDateTime convertToEntityAttribute(Timestamp dbData) {
        return dbData.toLocalDateTime();
    }
    
}
